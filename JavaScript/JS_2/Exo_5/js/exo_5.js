//chargement du DOM (check si la page est prête)
$(document).ready(function(){
    $("#lien").click(function(prmEvenement){
        //desactive la fonctionnement standard du lien
        prmEvenement.preventDefault();

        //remplace la balise <a> par une <img>
        $("#lien").replaceWith("<img src='img/tpinfo.jpg' alt=''/>");
    });

})