$(document).ready(function(){
//code a executer apres le chargement de la page html
$("#btnCalculImc").click(function(){
//Lire les valeurs saisies en poids et en taille  
let poids = $("#idPoids").val();
poids = poids.replace(",",".");
poids = Number(poids);
let taille = $("#idTaille").val();
taille = taille.replace(",",".");
taille = Number(taille);
if(isNaN(poids)||isNaN(taille)){
    //afficher l'erreur
    $("#textIMC").html("Saisie incorrecte");
}else{
    let imc = CalculerIMC(poids, taille / 100);
    //Affichage du resultat
    let texte = imc.toFixed(1);
    $("#textIMC").html(texte);
}
});
function CalculerIMC(prmPoids, prmTaille){
    //poids en kg et taille en cm
    let valRetour = prmPoids / (prmTaille*prmTaille);
    return valRetour;
}
});