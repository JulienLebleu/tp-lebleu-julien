$(document).ready(function() {
    //code a executer apres le chargement de la page html
    afficherIMC();
    $("#idSliderPoids").on('input', function() {
        //appel de la fonction
        afficherIMC();
    });

    $("#idSliderTaille").on('input', function() {
        //appel de la fonction
        afficherIMC();
    });

    //déclaration de fonction
    //fonction d'affichage de l'imc
    function afficherIMC() {
        //on récupère les valeurs saisies
        let poids = $("#idSliderPoids").val();
        let taille = $("#idSliderTaille").val();
        let imc = calculerIMC(poids, taille/100);
        let interpretation = interpreterIMC(imc);
        let aiguille = afficherBalance(imc);

        //affichage du poids à côté du slider
        $("#textPoids").html(poids);
        //affichage de la taille à côté du slider
        $("#textTaille").html(taille);
        //affichage de l'imc
        $("#textIMC").html(imc + " (" + interpretation + ")");
        //affichage de la balance
        $("#aiguille").css("left", aiguille + "px");

        //affichage de la silhouette
        afficherSilhouette(imc);
    }

    //fonction de calcul de l'imc
    function calculerIMC(prmPoids, prmTaille) {
        //calcul de l'imc
        let valRetour = prmPoids / (prmTaille*prmTaille);
        valRetour = valRetour.toFixed(1);

        return valRetour;
    }

    //fonction de l'interprétation de l'imc
    function interpreterIMC(prmValImc){
        let interpretation = "";
        //else if pour pas passer par tout les if et donc ca va plus vite
        if(prmValImc<16.5){
            interpretation="famine";
        }else if(prmValImc<18.5){
            interpretation="maigreur";
        }else if(prmValImc<25){
            interpretation="corpulence normal";
        }else if(prmValImc<30){
            interpretation="surpoids";
        }else if(prmValImc<35){
            interpretation="obésité modéré";
        }else if(prmValImc<40){
            interpretation="obésité sévère";
        }else{
            interpretation="obésité morbide";
        }
        return interpretation;
    }

    //fonction affichage de la balance
    function afficherBalance(prmValIMC) {
        if ((prmValIMC >= 10) && (prmValIMC <= 45)) {
            let deplacement = (60 / 7) * prmValIMC - (600 / 7); //en pixel

            return deplacement;
        }
    }

    //fonction qui gère l'affichage de la silhouette
    function afficherSilhouette(prmValIMC) {
        let decalage = ""; //decalage de l'img pour afficher différentes silhouette
        const tailleSilhouette = 105; //taille en pixel d'une silhouette

        //silhouette en fonction de l'imc
        if (prmValIMC<16.5) {
            decalage = 6 * tailleSilhouette;
        }else if(prmValIMC<18.5) {
            decalage = 5 * tailleSilhouette;
        }else if(prmValIMC<25)  {
            decalage = 4 * tailleSilhouette;
        }else if(prmValIMC<30)   {
            decalage = 3 * tailleSilhouette;
        }else if(prmValIMC<35) { 
            decalage = 2 * tailleSilhouette;
        }else{
            decalage = tailleSilhouette;
        }

        //affichage de la silhouette qui correspond à l'imc   
        $("#silhouette").css("background-position", decalage);
    }
});