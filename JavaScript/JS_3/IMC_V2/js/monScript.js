$(document).ready(function(){
    //code a executer apres le chargement de la page html
    $("#btnCalculImc").click(function(){
    //Lire les valeurs saisies en poids et en taille
    let poids = $("#idPoids").val();
    poids = poids.replace(",",".");
    poids = Number(poids);
    let taille = $("#idTaille").val();
    taille = taille.replace(",",".");
    taille = Number(taille);
    if(isNaN(poids)||isNaN(taille)){
        //afficher l'erreur
        $("#textIMC").html("Saisie incorrecte");
    }else{
        let imc = CalculerIMC(poids, taille / 100);
        //Affichage du resultat
        let texte = imc.toFixed(1);
        texte += " ("+ interpreterIMC(imc) +")";
        $("#textIMC").html(texte);
    }
    });

    //déclaration de fonction

    function CalculerIMC(prmPoids, prmTaille){
        //poids en kg et taille en cm
        let valRetour = prmPoids / (prmTaille*prmTaille);
        return valRetour;
    }

    function interpreterIMC(prmValImc){
        let interpretation = "";
        //else if pour pas passer par tout les if et donc ca va plus vite  
        if(prmValImc<16.5){
            interpretation="famine";
        }else if(prmValImc<18.5){
            interpretation="maigreur";
        }else if(prmValImc<25){
            interpretation="corpulence normal";
        }else if(prmValImc<30){
            interpretation="surpoids";
        }else if(prmValImc<35){
            interpretation="obésité modéré";
        }else if(prmValImc<40){
            interpretation="obésité sévère";
        }else{
            interpretation="obésité morbide";
        }
        return interpretation;
    }
    });