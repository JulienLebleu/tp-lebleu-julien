//Afficher la conversion de Euro(s) vers Dollar(s)
//Fait par Lebleu Julien

let valMax = prompt("Valeur maximale en euros : ");
affConvEuroDollar(valMax);


function affConvEuroDollar(prmValMax) {
    /*
    Afficher les conversions euros vers dollars Canadien
    jusqu'à prmValMax euros

    utilisation :
    affConvEuroDollar(10000);
    */
    const TAUX = 1.65; //taux de change 1€ = 1.65$
    let valeurEuro = 1;
    let valeurDollar = valeurEuro * TAUX;
    let affichage = "";
    //Boucle d'affichage
    while (valeurEuro <= prmValMax) {
        //gestion de l'affichage d'une ligne x euro = x dollars
        affichage = affichage + valeurEuro + " euro(s) = " + valeurDollar.toFixed(2) + " dollar(s)" + "\n";
        valeurEuro = valeurEuro * 2;
        valeurDollar = valeurEuro * TAUX;
    }
    //affichage final des conversions
    alert(affichage);
}
