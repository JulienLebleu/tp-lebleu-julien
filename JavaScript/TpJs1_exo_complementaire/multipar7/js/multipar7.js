//Afficher la table de multiplication d'un nombre entre par l'utilisateur.
//Fait par Lebleu Julien
let Table = prompt("Choisissez une table de multiplcation : ");             //demande à l'utilisateur la table de multiplication qu'il veut.
CalculerTableMulti(Table);                                                  //appel de la fonction

function CalculerTableMulti(prmTable){
    /*
    Afficher la table de multiplication d'un nombre entre par l'utilisateur (prmtable).
    
    utilisation :
    CalculerTableMulti(7);
    */
    let res = 0;
    let i = 0;
    let affichage = "";
    //Boucle d'affichage 
    while(i<20){
        res = prmTable*i;                                                   //calcul de la multiplication de 7 * i(0,1,2,3.. jusqu'à 20)   
        affichage = affichage + i + "*" + prmTable + "=" + res + "\n";      //gestion de l'affichage d'une ligne i * 7 = res 
        i++;
    }
    alert(affichage);               //valeur de retour
}