//Afficher 'e' si dans le texte saisi par l'utilisateur il y en a un
//Fait par Lebleu Julien

let phrase = prompt("Entrez une phrase : ");
let test = lettre(phrase);                              //appel de la fonction pour stocker si il y a un e ou non


//Affichage de la réponse si il y a un "e" ou non
if(test===true){                     
    alert("Le texte contient la lettre e");
}else{
    alert("Le texte ne contient pas la lettre e");
}


function lettre(phrase){
    /*
    Detecte si il y a un "e" dans la phrase 

    utilisation :
    lettre(il y a un e dans cette phrase);
    */
    let lettreE = false;                                        //valeur de retour 
    //boucle qui va tester toutes les lettres de la phrase pour voir si il y a un 'e'
    for(let i=0;i<phrase.length;i++){                           //length compte le nb de lettre dans la phrase
        if(phrase.charAt(i)==="e"){                             //charAt(i) ca retourne le caract n°i dans la phrase
            lettreE=true;
        }
    }
    return lettreE;
}