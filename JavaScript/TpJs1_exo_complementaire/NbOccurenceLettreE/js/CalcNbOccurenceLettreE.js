//Afficher 'e' si dans le texte saisi par l'utilisateur il y en a un
//Fait par Lebleu Julien

let phrase = prompt("Entrez une phrase : ");
let test = lettre(phrase);                              //appel de la fonction pour stocker le nb de e qu'il y a dans la phrase


//Affichage de la réponse cb il y a de "e"
if(test===0){         
    alert("Le texte ne contient pas la lettre e");            
}else{
    alert("Le texte contient " + test + " fois la lettre e");
}


function lettre(phrase){
    /*
    Detecte si il y a un "e" dans la phrase et compte le nombre de "e" 

    utilisation :
    lettre(il y a un e dans cette phrase);
    */
    let cpt = 0;                                                //compteur pour savoir cb de fois il y a de "e" et valeur d retour
    //boucle qui va tester toutes les lettres de la phrase pour voir si il y a un 'e'
    for(let i=0;i<phrase.length;i++){                           //length compte le nb de lettre dans la phrase
        if(phrase.charAt(i)==="e"){                             //charAt(i) ca retourne le caract n°i dans la phrase
            cpt++;                                              //incrementation du compteur
        }
    }
    return cpt;
}