//Afficher la table de multiplication d'un nombre entre par l'utilisateur. Et ca affiche une * quand c'est un multiple de 3
//Fait par Lebleu Julien
let Table = prompt("Choisissez une table de multiplcation : ");             //demande à l'utilisateur la table de multiplication qu'il veut.
CalculerTableMulti(Table);                                                  //appel de la fonction

function CalculerTableMulti(prmTable){
    /*
    Afficher la table de multiplication d'un nombre entre par l'utilisateur (prmtable). 
    Et afficher * quand c'est un multiple de 3 
    
    utilisation :
    CalculerTableMulti(7);
    */
    let res = 0;
    let i = 0;
    let affichage = "";
    //Boucle d'affichage 
    while(i<20){
        res = prmTable*i;                                                   //calcul de la multiplication de 7 * i(0,1,2,3.. jusqu'à 20) 
        if(res === 0){                                                      //si le resultat est 0 ca met obligatoirement une etoile
            res = res +"*";
        }  
        if((res%3===0)&&(res!==0)){                                             //si res de la div = 0 et reste = 0 par la division par 3 alors
            res = res + "*";                                                //ca affiche une etoile
        }
        affichage = affichage + i + "*" + prmTable + "=" + res + "\n";      //gestion de l'affichage d'une ligne i * 7 = res 
        i++;                                                                //ncrementation du compteur
    }
    alert(affichage);               //valeur de retour
}