//Calculer le volume d'un parallelepipede rectangle
//Fait par Lebleu Julien
let L = prompt("Entrez une largeur en mètre : ");
let H = prompt("Entrez une hauteur en mètre : ");
let P = prompt("Entrez une profondeur en mètre : ");

alert("Le volume du parallélépipède rectangle est de : " + CalculerVolume(L,H,P) + " mètre cube.");

function CalculerVolume(prmL, prmH, prmP){
    /*
    Calculer le volume d'un parallelepipede rectangle

    utilisation :
    CalculerVolume(10,5,3);
    */
   let res = 0;
   res = prmL*prmH*prmP;

   return res;
}