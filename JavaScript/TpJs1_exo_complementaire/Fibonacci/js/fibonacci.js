//Afficher les n premiers nombres de la suiet de Fibonacci
//Fait par Lebleu Julien
let NbAffSuite = prompt("Entrez le nombre de nombre que vous voulez de la suite de Fibonacci : ");
AffSuiteFibonacci(NbAffSuite);

function AffSuiteFibonacci(prmNbAff){
    /*
    Afficher les n premiers nombres de la suiet de Fibonacci
    jusqu'à prmNbAff

    utilisation :
    AffSuiteFibonacci(17);
    */

    let affichage = "";
    let var1 = 1;
    let var2 = 0;
    let res = 0;
    //Boucle d'affichage
    for(let i=0;i<prmNbAff;i++){
        if(i<=1){
            if(i==0){
                affichage = affichage + "0";
            }else{
                affichage = affichage + "-1";
            }
        }else{
            res = var1 + var2;
            affichage = affichage + "-" + res;
            var2 =var1;
            var1 = res;
        }
    }
    //affichage final de la suite de Fibonacci
    alert(affichage);
}