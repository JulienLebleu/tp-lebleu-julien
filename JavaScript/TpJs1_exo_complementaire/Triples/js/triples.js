//Afficher une suite de de n nombres dont chaque terme soit egal du triple du terme precedent
//Fait par Lebleu Julien
let NbAffichage = prompt("Choisissez le nombre de terme voulu : ");
AffTriple(NbAffichage);

function AffTriple(prmNbAff){
    /*
    Afficher une suite de de n nombres dont chaque terme soit egal du triple du terme precedent
    jusqu'à prmNbAff

    utilisation :
    AffTriple(12);
    */
    let affichage = "";
    let nb = 1;
    //Boucle d'affichage
    for(let i=0;i<prmNbAff;i++){
        //gestion de l'affichage en une ligne
        affichage = affichage + nb + " ";
        nb = nb * 3;
    }
    //affichage final 
    alert(affichage);
}
