//ce programme convertit un nombre entier de secondes saisi au départ en un nombre d’années, de
//mois, de jours, de minutes et de secondes
//Fait par Lebleu Julien
let seconde = prompt("Entrez un nombre de seconde : ");
alert(ConversionSeconde(seconde));

function ConversionSeconde(prmTemps){
    /*
    convertit un nombre entier de secondes saisi au départ en un nombre d’années, de
    mois, de jours, de minutes et de secondes

    utilisation :
    ConversionSeconde(1200000);
    */
   let annees =0;
   let mois = 0;
   let jours = 0;
   let heures = 0;
   let minutes = 0;
   let secondes = 0;
   let res = "";                                                //valeur de retour de la fonction 

   minutes = prmTemps/60;                                       //calcul des minutes en fonction des secondes données
   secondes = prmTemps%60;                                      //pour avoir le reste de la division pour savoir cb il reste de secondes 
   heures = minutes/60;                                         //calcul des heures en fonction des minutes
   minutes = minutes%60;                                        //pour avoir le reste de la division pour savoir cb il reste de minutes
   jours = heures/24;                                           //calcul des jours en fonction des heures 
   heures = heures%24;                                          //pour avoir le reste de la division pour savoir cb il reste d'heures
   mois = jours/30;                                             //calcul des mois en fonctiond des jours
   jours = jours%30;                                            //pour savoir le reste de la division pour savoir cb il reste de jours
   annees = mois/12;                                            //calcul des annees en fonction des mois
   mois = mois%12;                                              //pour avior le reste de la division pour savoir cb il reste de mois

   res = res + Math.trunc(annees) + " années, ";                //rajotue la partie entiere des annees dans la chaine de caractere
   res = res + Math.trunc(mois) + " mois, ";                    //rajotue la partie entiere des mois dans la chaine de caractere
   res = res + Math.trunc(jours) + " jours, ";                  //rajotue la partie entiere des jours dans la chaine de caractere
   res = res + Math.trunc(heures) + " heures, ";                //rajotue la partie entiere des heures dans la chaine de caractere
   res = res + Math.trunc(minutes) + " minutes, ";              //rajotue la partie entiere des minutes dans la chaine de caractere
   res = res + Math.trunc(secondes) + " secondes. ";            //rajotue la partie entiere des secondes dans la chaine de caractere

   return res;
}